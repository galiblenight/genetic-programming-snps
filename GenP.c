#ifndef __GENP__
#define __GENP__

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <signal.h>
#include <sys/time.h>
#include "essential.h"
#include "function_store.h"
#include "setting.c"
#include "init.c"

#define max(a,b) ((a > b)?a:b)
#define min(a,b) ((a > b)?b:a)
// #define inverseNxN(a, b, c, d) inverseNxN(a, b, c, d, matrix_dummy[thread]))

long time_col, sp;
int usage;

/* Use this function to initialize every dependency variable */
/* This function have no paramiter */
/* return None */
double COST_FUNCTION(double, int);

void startTime() {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  time_col = (tv.tv_sec*1000000) + tv.tv_usec;
}

long endTime(){
  struct timeval tv;
  gettimeofday(&tv, NULL);
  long t = (tv.tv_sec*1000000) + tv.tv_usec;
  return(t-time_col);
}

int compareRecentUsageMem(){
  int u = getMemUsageSize();
  int cmp = u - usage;
  usage = u;
  return cmp;
}

int getVariableCol(){
  return var_col;
}

int readdatafile(char *input_filename){
	//////// READ DATA FROM TXT FILE ////////
	FILE *file;
	size_t nread;
	int counter = 0, i, j, k, start_row = 0;
  var_row = -1;
  var_col = 0;
	char *buf = (char*)malloc(VARIABLE_ALLOCATE_SIZE);
	file = fopen(input_filename, "r");

	if (buf == NULL) {
	    printf("cannot malloc buf\n");
	    return 0;
    }

	int expect_index_i = 0;
	int expect_index_j = 0;
	if (file) {
		while ((nread = fread(buf, 1, VARIABLE_ALLOCATE_SIZE, file)) > 0) {
	        char *c = buf;
	        int state = 1, alloc = 0;
	        int i=0; //buffer index
	        while(i<nread){ //number of char read to buf (last time must lower than OTHER_OBJECT_ALLOCATE_SIZE)
	        	if(*(c+i) != '\t' && *(c+i) != '\r'){
              if(state == 1 && start_row == 1){
                if(expect_index_j < var_col){
                  if(expect_index_j == 0){
                    variable_env[expect_index_i] = malloc(VARIABLE_ALLOCATE_SIZE);
                  }
                  variable_env[expect_index_i][expect_index_j] = (double) *(c+i)-'0';
                }else{
                  expect_env[expect_index_i] = malloc(sizeof(double));
                  expect_env[expect_index_i][0] = (double) *(c+i)-'0';
                }
  	        		expect_index_j += 1;
  	        		counter++;
  	        		state=0;
              }
            if(!var_row){
                if(!alloc){
                  alloc = 1;
                  variable_name[var_col] = malloc(2000);
                  variable_name[var_col][0] = 0;
                }
                sprintf(variable_name[var_col], "%s%c", variable_name[var_col], *(c+i));
              }
	        	}
	        	if(*(c+i) == '\t'){ //first charactor of column name
	        		state=1;
              if(start_row == 0){
                var_col++;
                alloc = 0;
              }
	        	}
	        	if(*(c+i) == '\r'){ //Enter to new row
              var_row++;
	        		if(start_row==1){
	        			expect_index_i++;
	        			expect_index_j = 0;
	        		}else{
	        			start_row =1 ;
	        		}
	        		state=1;
	        	}
	        	i=i+1;
	        }
	        c = NULL;
		}
		if (ferror(file)) {
	    printf("cannot read file\n");
    	return 0;
    }
	  fclose(file);
	}
	free(buf);
  for(i = 0; i < NUM_THREADS; i++){
    for(j = 0; j < var_row; j++){
      input[i][j] = malloc(OBJECT_ALLOCATE_SIZE);
      o2[i][j] = malloc(OBJECT_ALLOCATE_SIZE);
    }
  }
  for(i = 0; i < var_row; i++){
    dom_variable_env[i] = malloc(var_col*sizeof(double));
    add_variable_env[i] = malloc(var_col*sizeof(double));
    for(j = 0; j < var_col; j++){
      for(k = 0; k < extract_inp_size; k++){
        if(variable_env[i][j] == extract_inp[k]){
          dom_variable_env[i][j] = extract_to[k][0];
          add_variable_env[i][j] = extract_to[k][1];
          break;
        }
      }
    }
  }
  evaluateCostTerminal(VARIABLE_COST_ESTIMATE_TIMES, variable_env, variable_cost, variable_prop,
                       &max_variable_cost, &min_variable_cost, &sum_variable_prop);
  evaluateCostTerminal(VARIABLE_COST_ESTIMATE_TIMES, dom_variable_env, dom_variable_cost, dom_variable_prop,
                       &max_dom_variable_cost, &min_dom_variable_cost, &sum_dom_variable_prop);
  evaluateCostTerminal(VARIABLE_COST_ESTIMATE_TIMES, add_variable_env, add_variable_cost, add_variable_prop,
                       &max_add_variable_cost, &min_add_variable_cost, &sum_add_variable_prop);
	return counter;
}

int getMemUsageSize(){
  FILE* status = fopen( "/proc/self/status", "r" );
  char *buf = malloc(5000), str[20];
  size_t nread;
  int i, j = 0;
  if ((nread = fread(buf, 1, VARIABLE_ALLOCATE_SIZE, status)) > 0) {
    i = indexStrOf(buf, "VmSize:")+7;
    while(buf[++i] == ' ');
    i--;
    while(buf[++i] != ' '){
      str[j++] = buf[i];
    }
    str[j] = 0;
  }
  fclose(status);
  free(buf);
  return atoi(str);
}

int print_stack_pointer() {
  char *p = malloc(1);
  long ret = ((long)p)-sp-32;
  sp = (long)p;
  // free(p);
  return ret;
}

int getMemUsagePeak(){
  FILE* status = fopen( "/proc/self/status", "r" );
  char *buf = malloc(5000), str[20];
  size_t nread;
  int i, j = 0;
  if ((nread = fread(buf, 1, VARIABLE_ALLOCATE_SIZE, status)) > 0) {
    i = indexStrOf(buf, "VmPeak:")+7;
    while(buf[++i] == ' ');
    i--;
    while(buf[++i] != ' '){
      str[j++] = buf[i];
    }
    str[j] = 0;
  }
  fclose(status);
  free(buf);
  return atoi(str);
}

void evaluateCostTerminal(int eporch, double **var, double *var_cost, double *var_prop, double *max_var_cost,
                          double *min_var_cost, double *sum_var_prop){
  int i, j, k;
  ones(var_row, 1, m_one[0]);
  if(t_theta_var_size[0] < 2){
    t_theta_var_size[0] = 2;
    t_theta[0][0] = malloc(OBJECT_ALLOCATE_SIZE);
    t_theta[0][1] = malloc(OBJECT_ALLOCATE_SIZE);
  }
  if(Jpp_var_size[0] < 2){
    Jpp_var_size[0] = 2;
    Jpp[0][0] = malloc(OBJECT_ALLOCATE_SIZE);
    Jpp[0][1] = malloc(OBJECT_ALLOCATE_SIZE);
  }
  if(t_input_var_size[0] < 2){
    t_input[0][0] = malloc(OBJECT_ALLOCATE_SIZE);
    t_input[0][1] = malloc(OBJECT_ALLOCATE_SIZE);
    t_input_var_size[0] = 2;
  }
  for(i = 0; i < var_col; i++){
    t_theta[0][0][0] = 0.0;
    t_theta[0][1][0] = 0.0;
    for(k = 0; k < var_row; k++){
      input[0][k] = malloc(OBJECT_ALLOCATE_SIZE);
      input[0][k][0] = 1.0;
      input[0][k][1] = var[k][i];
      t_input[0][0][k] = 1.0;
      t_input[0][1][k] = var[k][i];
    }
    for(j = 0; j < eporch; j++){
      xmatrix(input[0], var_row, 2, t_theta[0], 2, 1, x_input[0]);
      sigmoid_mat(x_input[0], var_row, 1, output[0]);
      minus_mat(output[0], expect_env, var_row, 1, o0[0]);
      xmatrix(t_input[0], 2, var_row, o0[0], var_row, 1, Jp[0]);
      minus_mat(m_one[0], output[0], var_row, 1, moutput[0]);
      for(k = 0; k < var_row; k++){
        moutput[0][k][0] = output[0][k][0] * moutput[0][k][0];
      }
      diag(moutput[0], var_row, o1[0]);
      xmatrix(o1[0], var_row, var_row, input[0], var_row, 2, o2[0]);
      xmatrix(t_input[0], 2, var_row, o2[0], var_row, 2, r_input[0]);
      if(inverse2x2(r_input[0], Jpp[0]) == 0){
        continue;
      }
      Jpp_var_size[0] = 0;
      xmatrix(Jpp[0], 2, 2, Jp[0], 2, 1, grad[0]);
      t_theta[0][0][0] = t_theta[0][0][0] - grad[0][0][0];
      t_theta[0][1][0] = t_theta[0][1][0] - grad[0][1][0];
    }
    var_cost[i] = 0.0;
    for(k = 0; k < var_row; k++){
      var_cost[i] -= expect_env[k][0] * log(output[0][k][0]) + ((1 - expect_env[k][0]) * log(1 - output[0][k][0]));
    }
    var_cost[i] = COST_FUNCTION(var_cost[i], 1);
    if(i == 0){
      (*max_var_cost) = var_cost[i];
      (*min_var_cost) = var_cost[i];
      var_prop[0] = 0.0;
    }else{
      if(var_cost[i] > (*max_var_cost)){
        (*max_var_cost) = var_cost[i];
      }else if(var_cost[i] < (*min_var_cost)){
        (*min_var_cost) = var_cost[i];
      }
    }
  }
  double top = (*max_var_cost) + (*min_var_cost);
  for(i = 1; i < var_col; i++){
    double prop = top - var_cost[i-1];
    var_prop[i] = var_prop[i-1] + prop;
    (*sum_var_prop) += prop;
  }
}

void fitTreeSize(char** tree, int *tree_size){
  int tz =(*tree_size);
  int i = tz, j = 0, depth = (int)(log(tz+1)/log(2))-1;
  while(tree[i-1][0] == 0)
    i--;
  i = tz - i;
  while((i = i - powInt(2, depth)) >= 0){
    j += powInt(2, depth--);
  }
  (*tree_size) -= j;
}

int randomStrAsymBinary(int func_set_size, int term_set_size, int var_set_size, int cost_var_base,
                        int func_size, int max_depth, int uni_term_func_size, char** Tree,
                        int *TreeSize, char** Postfix ,int *PostfixSize, int *TreeIndex){
  (*PostfixSize) = 0;
  int i, j;
  int *consider = malloc(10000), *terminal = malloc(10000);
  int consider_var_size, terminal_var_size;
  int last_func_index = powInt(2, max_depth-2)-1, ad;
  int randvalue, randindex, child, tv_size = term_set_size+var_set_size, tree_size = powInt(2, max_depth)-1;
  int postfixsize = (2*(func_size))+1, function_remain = powInt(2, max_depth-1) - func_size - 1;
  consider_var_size = 1, terminal_var_size = 0;
  (*TreeSize) = powInt(2, max_depth)-1;
  for(i = 0; i < (*TreeSize); i++){
    Tree[i][0] = 0;
  }
  int randnum = getRandomFuncBinary(uni_term_func_size+1, func_set_size, &function_remain, 0, max_depth);
  uni_term_func_size--;
  consider[0] = 0;
  for(i = 0; i < func_size; i++){
    randindex = randint(0, consider_var_size-1);
    randvalue = consider[randindex];
    child = (randvalue*2)+1;
    // randnum = getRandomFuncBinary(uni_term_func_size, func_set_size, &function_remain, i, max_depth);
    randnum = randint(0, func_set_size-1);
    // Tree[randvalue] = malloc(2+tostrlen(randnum));
    sprintf(Tree[randvalue], "f%d", randnum);
    if(randvalue < last_func_index){
      appendArray(consider, &consider_var_size, child);
      if(randnum > uni_term_func_size){
        appendArray(consider, &consider_var_size, child+1);
      }
    }else{
      appendArray(terminal, &terminal_var_size, child);
      if(randnum > uni_term_func_size){
        appendArray(terminal, &terminal_var_size, child+1);
      }
    }
    if(randnum <= uni_term_func_size){
      postfixsize--;
    }
    popArray(consider, &consider_var_size, randindex);
  }
  for(i = 0; i < terminal_var_size; i++){
    appendArray(consider, &consider_var_size, terminal[i]);
  }
  while(terminal_var_size > 0){
    popArray(terminal, &terminal_var_size, terminal_var_size-1);
  }
  for(i = 0; i < var_set_size; i++){
    appendArray(terminal, &terminal_var_size, i);
  }
  while(consider_var_size > 0){
    randindex = randint(0, consider_var_size-1);
    randvalue = consider[randindex];
    child = (randvalue*2)+1;
    if(randnum < var_set_size){
      while(1){
        int neighbor = randvalue + (randvalue%2==1?1:-1);
        ad = randint(0, 1);
        if(ad){
          double rn = randrange(0.0, sum_dom_variable_prop);
          for(i = 0; i < var_col; i++){
            if(dom_variable_prop[i] > rn)
              break;
          }
        }else{
          double rn = randrange(0.0, sum_add_variable_prop);
          for(i = 0; i < var_col; i++){
            if(add_variable_prop[i] > rn)
              break;
          }
        }
        if(Tree[neighbor][0] == 0){
          break;
        }
        if((Tree[neighbor][0] == 'v' && !ad) || (Tree[neighbor][0] == 'V' && ad)){
          if(i - 1 != getIntBinary(Tree[neighbor]))
            break;
        }else{
          break;
        }
      }
      // Tree[randvalue] = malloc(2+tostrlen(i-1));
      if(ad){
        sprintf(Tree[randvalue], "V%d", i-1);
      }else{
        sprintf(Tree[randvalue], "v%d", i-1);
      }
    }else{
      randnum = randint(0, tv_size-1);
      // Tree[randvalue] = malloc(2+tostrlen(randnum-var_set_size));
      sprintf(Tree[randvalue], "t%d", randnum-var_set_size);
    }
    popArray(consider, &consider_var_size, randindex);
  }
  int start = powInt(2, max_depth-1)-1, pointer = start, nextleaf = start, set_index = 0;
  while(pointer >= 0){
    if(Tree[pointer][0] != 0){
      // PyList_SetItem(returnPostfix, set_index, Py_BuildValue("s", _tree[pointer]));
      // Postfix[set_index] = malloc(strlen(Tree[pointer]));
      sprintf(Postfix[set_index] , "%s", Tree[pointer]);
      set_index++;
    }
    if(pointer >= start){
      if(pointer%2 == 1){
        pointer += 1;
      }else{
        nextleaf += 2;
        pointer = (pointer-2)/2;
      }
    }else{
      if(pointer%2 == 1){
        pointer = nextleaf;
      }else{
        pointer = pointer = (pointer-2)/2;
      }
    }
  }
  (*PostfixSize) = set_index;
  set_index = 0;
  for(i = 0; i < tree_size; i++){
    if(Tree[i][0] != 0){
      TreeIndex[set_index] = i;
      set_index++;
    }
  }
  free(consider);
  free(terminal);
}

double AIC(double cross_entropy, int tree_size){
  double result = (2.0 / var_row) * cross_entropy + 2.0 * tree_size / var_row;
  return result;
}

double BIC(double cross_entropy, int tree_size){
  double result = 2.0 * cross_entropy + log(var_row)/log(exp(1)) * tree_size;
  return result;
}

double NoneFunc(double cross_entropy, int tree_size){
  return cross_entropy;
}

/* Use this function to get 2 crossover offspring of 2 input tree */
/* get 5 param (1D_array_string_list, int, 1D_array_string_list, int, int)
      1. string list of first parent
      2. integer of first parent size
      3. string list of second parent
      4. integer of second parent size
      5. integer position of crossover */
/* return list of object of child tree and child index of each offspring */
void crossoverSetPoint(char** fparent, int* fparent_size, char** sparent, int *sparent_size, int crossPoint,\
                                   char** fchild, int* fchild_size, int* fchildIndex, char **fpostfix, int *fpostfix_size,\
                                   char** schild, int* schild_size, int* schildIndex, char **spostfix, int *spostfix_size){
  /*initial condition*/
  (*fchild_size) = 0;
  (*schild_size) = 0;
  /////////////////////
  int i, j, k;
  int parentSize[] = {(*fparent_size), (*sparent_size)};
  int maxSize = max((*fparent_size), (*sparent_size)), currentdepth = (int)(log(crossPoint+1)/log(2)), **childIndex;
  char ***child, ***parent;
  (*fchild_size) = maxSize;
  (*schild_size) = maxSize;
  child = malloc(2*sizeof(char**));
  parent = malloc(2*sizeof(char**));
  childIndex = malloc(2*sizeof(int*));
  child[0] = fchild;
  child[1] = schild;
  parent[0] = fparent;
  parent[1] = sparent;
  childIndex[0] = fchildIndex;
  childIndex[1] = schildIndex;
  int child_index_size[] = {0, 0};
  for(i = 0; i < 2; i++){
    int pointer = crossPoint, jump = crossPoint, use = 1;
    for(j = 0; j < maxSize; j++){
      if(j == pointer){
        pointer += use+jump;
        jump *= 2;
        for(k = 0; k < use; k++){
          if(j >= parentSize[!i]){
            child[i][j][0] = 0;
          }else{
            if(parent[!i][j][0] != 0){
              sprintf(child[i][j], "%s", parent[!i][j]);
            }else{
              child[i][j][0] = 0;
            }
          }
          j++;
        }
        j--;
        use *= 2;
      }else{
        if(j >= parentSize[i]){
          child[i][j][0] = 0;
        }else{
          if(parent[i][j][0] != 0){
            sprintf(child[i][j], "%s", parent[i][j]);
          }else{
            child[i][j][0] = 0;
          }
        }
      }
    }
  }
  if(!SEPARATE_ADD_DOM){
    for(i = 0; i < 2; i++){
      int neighbor = crossPoint + ((crossPoint%2)?1:-1), root = (int)((crossPoint-1)/2);
      if(root >= 0){
        if(child[i][crossPoint][0] == 'f' && child[i][neighbor][0] == 'f'){
          sprintf(child[i][root], "f%d", randint(0, ff-1));
        }else if(child[i][crossPoint][0] == 'f' || child[i][neighbor][0] == 'f'){
          sprintf(child[i][root], "f%d", randint(0, fv-1));
        }else{
          sprintf(child[i][root], "f%d", randint(0, vv-1));
        }
      }
    }
  }
  for(i = 0; i < 2; i++){
    int size = 0;
    for(j = 0; j < maxSize; j++){
      if(child[i][j][0] != 0){
        appendArray(childIndex[i], &size, j);
      }
    }
  }
  free(child);
  free(parent);
  free(childIndex);
  parsePostfixBinary(fchild, fchild_size, fpostfix, fpostfix_size);
  parsePostfixBinary(schild, schild_size, spostfix, spostfix_size);
}

void crossoverWorstToBest(char **fparent, int *fparent_size, int fworst, int fbest, char** sparent, int *sparent_size,\
                          int sworst, int sbest, char** fchild, int* fchild_size, int* fchildIndex, char **fpostfix, int *fpostfix_size,\
                          char** schild, int* schild_size, int* schildIndex, char **spostfix, int *spostfix_size){
  char ***parent = malloc(2*sizeof(char**));
  parent[0] = fparent;
  parent[1] = sparent;
  char ***child = malloc(2*sizeof(char**));
  child[0] = fchild;
  child[1] = schild;
  int i, j, k, worst[2] = {fworst, sworst}, best[2] = {fbest, sbest};
  int parentSize[2] = {(*fparent_size), (*sparent_size)}, child_index_size[] = {0, 0};
  int **childIndex = malloc(2*sizeof(int*));
  childIndex[0] = fchildIndex;
  childIndex[1] = schildIndex;
  int parentDepth[2] = {(int)(log(parentSize[0]+1)/log(2)), (int)(log(parentSize[1]+1)/log(2))};
  int outputDepth[2];
  for(i = 0; i < 2; i++){
    worst[i] = getTreeIndexFromPostfixIndex(parent[i], parentDepth[i], worst[i]);
    best[i] = getTreeIndexFromPostfixIndex(parent[i], parentDepth[i], best[i]);
  }
  for(i = 0; i < 2; i++){
    int w_depth = (int)(log(worst[i]+1)/log(2));
    int b_depth = (int)(log(best[!i]+1)/log(2));
    outputDepth[i] = (parentDepth[!i] - b_depth) - (parentDepth[i] - w_depth);
    if(outputDepth[i] > 0){
      outputDepth[i] = parentDepth[i] + outputDepth[i];
    }else{
      outputDepth[i] = parentDepth[i];
    }
  }
  int maxDepth = max(outputDepth[0], outputDepth[1]), maxSize = powInt(2, maxDepth)-1;
  for(i = 0; i < 2; i++){
    int pointer[2] = {worst[i], best[!i]}, jump[2] = {worst[i], best[!i]}, use = 1;
    for(j = 0; j < maxSize; j++){
      if(j == pointer[0]){
        for(k = 0; k < use; k++){
          if(pointer[1]+k >= parentSize[!i]){
            child[i][j][0] = 0;
          }else{
            sprintf(child[i][j], "%s", parent[!i][pointer[1]+k]);
          }
          j++;
        }
        pointer[0] += use+jump[0];
        jump[0] *= 2;
        pointer[1] += use+jump[1];
        jump[1] *= 2;
        j--;
        use *= 2;
      }else{
        if(j >= parentSize[i]){
          child[i][j][0] = 0;
        }else{
          sprintf(child[i][j], "%s", parent[i][j]);
        }
      }
    }
  }
  (*fchild_size) = maxSize;
  (*schild_size) = maxSize;
  for(i = 0; i < 2; i++){
    int size = 0;
    for(j = 0; j < maxSize; j++){
      if(child[i][j][0] != 0){
        appendArray(childIndex[i], &size, j);
      }
    }
  }
  free(parent);
  free(child);
  free(childIndex);
  parsePostfixBinary(fchild, fchild_size, fpostfix, fpostfix_size);
  parsePostfixBinary(schild, schild_size, spostfix, spostfix_size);
}


void parsePostfixBinary(char **Tree, int *TreeSize, char **Postfix, int *PostfixSize){
  int size = (*TreeSize), i, j, depth = (int)(log(size+1)/log(2));
  int start = powInt(2, depth - 1)-1, next = start, pointer = start, current_index = 0;
  while(pointer >= 0){
    if(Tree[pointer][0] != 0){
      sprintf(Postfix[current_index], "%s", Tree[pointer]);
      current_index++;
    }
    if(pointer >= start){
      if(pointer % 2 == 0){
        pointer = (pointer/2) - 1;
        next += 2;
      }else{
        pointer++;
      }
    }else{
      if(pointer % 2 == 0){
        pointer = (pointer / 2) - 1;
      }else{
        pointer = next;
      }
    }
  }
  (*PostfixSize) = current_index;
}

// cost_size = func_cost_index_size + var_cost_index_size
// Traditional flat function
int operateFlatFunctionWithLearning(int eporch, char **postfix, int *postfix_size, double leaf_cost, double *Accuracy,\
                                     double *cost, int *True_pos, int *False_pos, int thread){
  // printRecentUsageMem("mem usage a: ");
  int i = 0, j = 0, k = 0, l = 0, m = 0, n = 0, o = 0, p = 0, num = 0, sum = 0, post_size, func_size;
  double var1, var2;
  double leaf_size;
  int t_remaining, count_post_pointer;
  char str[200];
  post_size = (*postfix_size);
  func_size = (int)(post_size/2);
  int VarCostIndex_pointer = 0, FuncCostIndex_pointer = 0, comb_size = 0;
  ones(var_row, 1, m_one[thread]);
  int stack_pointer = 0, theta_pointer = 0;
  int interact[20], interact_size = 0;
  for(j = 0; j < post_size; j++){
    if(postfix[j][0] == 'f'){
      int leaf_count = 2, pointer = j, inputsize = 1, subtree_size = 1;
      int insert_pointer, index, num_object_comb = 1;
      while(leaf_count > 0){
        if(postfix[--pointer][0] == 'f'){
          leaf_count++;
        }else{
          leaf_count--;
        }
        subtree_size++;
      }
      for(i = 0; i < var_row; i++){ // set first input to 1.0
        input[thread][i][0] = 1.0;
      }
      comb_size = pf2pfcomb(postfix, j+1, comb[thread], thread, 1);
      for(k = 0; k < comb_size; k++){
        l = 0;
        num = (comb[thread][k][l] == 'V')?1:0;
        m = 0;
        interact_size = 0;
        while (comb[thread][k][++l] != 0) {
          if((comb[thread][k][l+1] == 0 && comb[thread][k][l] != '*') || ((comb[thread][k][l] == 'v' ||
              comb[thread][k][l] == 'V' || comb[thread][k][l] == '*' || comb[thread][k][l+1] == 0) && m > 0)){
            if(comb[thread][k][l+1] == 0 && comb[thread][k][l] != '*'){
              str[m++] = comb[thread][k][l];
            }else if(comb[thread][k][l] == 'V'){
              num = 1;
            }else if(comb[thread][k][l] == 'v'){
              num = 0;
            }
            str[m++] = 0;
            o = atoi(str);
            p = 1;
              if(num == 0){
                for(n = 0; n < var_row; n++){
                  input[thread][n][inputsize] = add_variable_env[n][o];
                }
              }else{
                for(n = 0; n < var_row; n++){
                  input[thread][n][inputsize] = dom_variable_env[n][o];
                }
              }
              inputsize++;
            // }
            m = 0;
          }
          if(comb[thread][k][l] == 'v' || comb[thread][k][l] == 'V'){
            if(comb[thread][k][l] == 'v'){
              num = 1;
            }else if(comb[thread][k][l] == 'V'){
              num = 0;
            }
            num_object_comb++;
          }else if(comb[thread][k][l] >= '0' && comb[thread][k][l] <= '9'){
            str[m++] = comb[thread][k][l];
          }else if(comb[thread][k][l] == '*'){
              for(n = 0; n < var_row; n++){
                var2 = input[thread][n][inputsize-1];
                var1 = input[thread][n][inputsize-2];
                input[thread][n][inputsize-2] = var1*var2;
              }
              inputsize--;
          }
        }
      }
      for(k = 0; k < inputsize; k++){
        cur_theta[thread][0][k] = 0.0;
      }
      for(i = 0; i < eporch; i++){
        transpost(cur_theta[thread], 1, inputsize, t_theta[thread]);
        transpost(input[thread], var_row, inputsize, t_input[thread]);
        xmatrix(input[thread], var_row, inputsize, t_theta[thread], inputsize, 1, x_input[thread]);
        sigmoid_mat(x_input[thread], var_row, 1, output[thread]);
        minus_mat(output[thread], expect_env, var_row, 1, o0[thread]);
        xmatrix(t_input[thread], inputsize, var_row, o0[thread], var_row, 1, Jp[thread]);
        minus_mat(m_one[thread], output[thread], var_row, 1, moutput[thread]);
        for(k = 0; k < var_row; k++){
          moutput[thread][k][0] = output[thread][k][0] * moutput[thread][k][0];
        }
        for(k = 0; k < var_row; k++){
          for(l = 0; l < inputsize; l++){
            o2[thread][k][l] = moutput[thread][k][0] * input[thread][k][l];
          }
        }
        o2_var_size[thread] = inputsize;
        xmatrix(t_input[thread], inputsize, var_row, o2[thread], var_row, inputsize, r_input[thread]);
        if(inverseNxN(r_input[thread], inputsize, Jpp[thread], matrix_dummy[thread]) == 0){
          double this_cost = -INFINITY;
          goto conclusion;
        }
        xmatrix(Jpp[thread], inputsize, inputsize, Jp[thread], inputsize, 1, grad[thread]);
        for(k = 0; k < inputsize; k++){
          cur_theta[thread][0][k] -= grad[thread][k][0];
        }
        transpost(cur_theta[thread], 1, inputsize, t_theta[thread]);
        xmatrix(input[thread], var_row, inputsize, t_theta[thread], inputsize, 1, x_input[thread]);
        sigmoid_mat(x_input[thread], var_row, 1, output[thread]);
        if(i == eporch - 1){
          double this_cost = 0;
          conclusion:
          for(k = 0; k < var_row; k++){
            this_cost -= expect_env[k][0] * log(output[thread][k][0]) + ((1 - expect_env[k][0]) * log(1 - output[thread][k][0]));
          }
          leaf_size = 0;
          t_remaining = 2;
          count_post_pointer = j;
          // find subtree
          while(t_remaining > 0){
            if(postfix[--count_post_pointer][0] == 'f'){
              t_remaining += 1;
            }else{
              t_remaining -= 1;
              leaf_size += leaf_cost;
            }
          }
          this_cost += ((double)inputsize)*leaf_cost;
          if(isinf(this_cost) || isnan(this_cost)){
            this_cost = -1;
          }
          int predict_correct = 0;
          if(j == post_size-1){
            (*True_pos) = 0;
            (*False_pos) = 0;
          }
          for(k = 0; k < var_row; k++){
            int ex_out = expect_env[k][0] >= 0.5;
            int out = output[thread][k][0] >= 0.5;
            predict_correct += (ex_out ? out : !out);
            if(j == post_size-1){
              if(ex_out){
                if(out){
                  (*True_pos)++;
                }else{
                  (*False_pos)++;
                }
              }
            }
          }
          (*Accuracy) = (double)(predict_correct * 100.0 / var_row);
          if(this_cost >= 0.0){
            cost[j] = COST_FUNCTION(this_cost, subtree_size-1);
          }else{
            cost[j] = -1;
          }
        }
      }
    }else{
      if(postfix[j][0] == 'v'){
        cost[j] = add_variable_cost[getIntBinary(postfix[j])];
      }else if(postfix[j][0] == 'V'){
        cost[j] = dom_variable_cost[getIntBinary(postfix[j])];
      }
    }
  }
}

void freeArray(double **array, int var_size){
  int i;
  for(i = var_size-1; i >= 0; i--){
    free(array[i]);
  }
  free(array);
}

int findIndexMax(int size, int start_at, double *value){
  double max = value[start_at];
  int i, max_index = start_at;
  for(i = start_at+1; i < size; i++){
    double v = value[i];
    if(max < v){
      max = v;
      max_index = i;
    }
  }
  return max_index;
}

int findIndexMin(int size, int start_at, double *value){
  double min = value[start_at];
  int i, min_index = start_at;
  for(i = start_at+1; i < size; i++){
    double v = value[i];
    if(min > v){
      min = v;
      min_index = i;
    }
  }
  return min_index;
}

void sort_by_error(double* error_array,int* return_index_array,int size){
  int count;
  double *cp_error_array = malloc(OBJECT_ALLOCATE_SIZE);
  for(count = 0; count < size; count++){
    cp_error_array[count] = error_array[count];
    return_index_array[count] = count;
  }

  quicksort(cp_error_array, 0, size-1, return_index_array);
  free(cp_error_array);
}

void quicksort(double* error_array, int first, int last, int* return_index_array){
     int pivot,j,i;
     double temp,temp2;

     if(first < last){
         pivot = first;
         i = first;
         j = last;

         while(i < j){
             while(error_array[i] <= error_array[pivot] && i < last){
                 i++;
             }while(error_array[j] > error_array[pivot]){
                 j--;
             }
             if(i < j){
                  temp  = error_array[i];
                  temp2 = return_index_array[i];
                  error_array[i]        = error_array[j];
                  return_index_array[i] = return_index_array[j];
                  error_array[j]        = temp;
                  return_index_array[j] = temp2;
             }
         }

         temp   = error_array[pivot];
         temp2  = return_index_array[pivot];
         error_array[pivot]       = error_array[j];
         return_index_array[pivot] = return_index_array[j];
         error_array[j]       = temp;
         return_index_array[j] = temp2;
         quicksort(error_array, first, j-1, return_index_array);
         quicksort(error_array, j+1, last, return_index_array);
    }
}//http://www.cquestions.com/2008/01/c-program-for-quick-sort.html

int stochastic(int *index, double *cost, int size, int *selected, int *selected_size){
  int i, j, k, l;
  double maxcost = cost[findIndexMax(size, 0, cost)];
  double *fitness = malloc(OBJECT_ALLOCATE_SIZE);
  // map to fitness
  for(i = 0; i < size; i++){
    fitness[i] = maxcost - cost[i];
  }

  double sum_fitness = 0.0;
  // sum
  for(i = 0; i < size; i++){
    sum_fitness += fitness[i];
  }
  // fitness scailing
  double f_max = fitness[findIndexMax(size, 0, fitness)];
  double f_min = fitness[findIndexMin(size, 0, fitness)];
  double f_avg = sum_fitness / size;
  double delta, a, b;
  if(f_min > (C_MULT * f_avg - f_max)/(C_MULT - 1.0)){
    delta = f_max - f_avg;
    a = (C_MULT - 1.0) * f_avg / delta;
    b = f_avg * (f_max - C_MULT * f_avg) / delta;
  }else{
    delta = f_avg - f_min;
    a = f_avg / delta;
    b = - f_min * f_avg / delta;
  }
  for(i = 0; i < size; i++){
    fitness[i] = a * fitness[i] + b;
  }
  /////////////////////////
  sum_fitness = 0.0;
  for(i = 0; i < size; i++){
    sum_fitness += fitness[i];
  }
  if(sum_fitness == 0){
    (*selected_size) = 0;
    return 0;
  }
  f_avg = sum_fitness / MAX_STOCHASTIC_SIZE;
  double pointer = randrange(0.0, sum_fitness);
  double start = pointer, sum_count = 0;
  int restart = 0, count = 0, previous = -1;
  int parent_size = 0;
  for(i = 0; i < size; i++){
    sum_count += fitness[index[i]];
    if(sum_count > pointer){
      break;
    }
    count++;
  }
  sum_fitness = 0.0;
  for(i = 0; i < size; i++){
    sum_fitness += fitness[i];
  }
  while((!restart || pointer < start) && parent_size < POPULATION_SIZE){
    if(count != previous){
      previous = count;
      selected[parent_size++] = index[count];
    }
    pointer += f_avg;
    if(pointer >= sum_fitness){
      if(restart){
        break;
      }else{
        count = 0;
        sum_count = fitness[index[0]];
        pointer -= sum_fitness;
        restart = 1;
      }
    }
    if(pointer > sum_count){
      count++;
      sum_count += fitness[index[count]];
    }
  }
  (*selected_size) = parent_size;
  free(fitness);
  return 1;
}

int sigmoid_function_generate(char **set, int set_size, char *mapTo){
  char **function_format;
  if(!strcmp(mapTo, "ff")){
    ff_format = malloc(BLUK_OBJECT_ALLOCATE_SIZE);
    function_format = ff_format;
  }else if(!strcmp(mapTo, "fv")){
    fv_format = malloc(BLUK_OBJECT_ALLOCATE_SIZE);
    function_format = fv_format;
  }else if(!strcmp(mapTo, "vv")){
    vv_format = malloc(BLUK_OBJECT_ALLOCATE_SIZE);
    function_format = vv_format;
  }
  int powerset_size = powInt(2,set_size);
  unsigned int i, j, counter;
  int temp = counter, a = 0, b = 0;
  for(i = 0; i < set_size; i++){
    j = 0;
    while(set[i][j] != 0){
      if(set[i][j] == 'a' || set[i][j] == 'A')
        a |= powInt(2, i);
      if(set[i][j] == 'b' || set[i][j] == 'B')
        b |= powInt(2, i);
      j++;
    }
  }
  for(i = 0; i < powerset_size; i++){
    function_format[i] = malloc(OBJECT_ALLOCATE_SIZE);
  }
  char* function_string = (char*)malloc(VARIABLE_ALLOCATE_SIZE);
  /*Run from counter 000..0 to 111..1*/
  for(i = 0; i < powerset_size; i++){
    if(i & a && i & b){ //index 10,9,...,0
      int length = 0;
      int loop_count=0;
      length+=sprintf(function_string+length, "u+", set[j]);
      for(j = 0; j < set_size; j++){
        if(i & (1<<j)){
          length+=sprintf(function_string+length, "%s+", set[j]);
          loop_count++;
        }
      }
      function_string[length] = '\0'; // END OF STRING
      sprintf(function_format[counter-temp], "%s", function_string);
      counter++;
    }
  }
  if(!strcmp(mapTo, "ff")){
    ff = counter-temp;
  }else if(!strcmp(mapTo, "fv")){
    fv = counter-temp;
  }else if(!strcmp(mapTo, "vv")){
    vv = counter-temp;
  }
  return counter-temp;
}

void crossover_bestfit(GPTree* tree, int* parent, int* parent_var_size, GPTree* offspring, int* offspring_size){
  int pri = 0;
  int sec = 0;
  int crosspoint = -1;
  int off_var_size = 0;
  int i, j;
  while((*parent_var_size) > 1){
    pri = popArray(parent, parent_var_size, randint(0,(*parent_var_size))); //first parent index
    sec = popArray(parent, parent_var_size, randint(0,(*parent_var_size))); //seccond parent index
    if((tree[pri].best_cost < tree[sec].worst_cost && tree[sec].best_cost < tree[pri].worst_cost) ||
        (tree[pri].worst_node == 0 || tree[sec].worst_node == 0)){ //best_cost(expect tree,expect tree size)
      //crossover set point
      int crosspoint = -1;
      while(crosspoint == -1){
        crosspoint = tree[pri].tree_index[randint(1, tree[pri].postfix_size-1)];
        if(indexOf(tree[sec].tree_index, &tree[sec].postfix_size, crosspoint) == -1){ // there're NOT this index in seccond tree
          crosspoint = -1;
        }
      }
      crossoverSetPoint(tree[pri].tree, &tree[pri].tree_size, tree[sec].tree, &tree[sec].tree_size, crosspoint,\
                        offspring[off_var_size].tree, &offspring[off_var_size].tree_size, offspring[off_var_size].tree_index, offspring[off_var_size].postfix, &offspring[off_var_size].postfix_size,\
                        offspring[off_var_size+1].tree, &offspring[off_var_size+1].tree_size, offspring[off_var_size+1].tree_index, offspring[off_var_size+1].postfix, &offspring[off_var_size+1].postfix_size);
    }else{
      // crossover worst - best
      if(tree[sec].best_cost < tree[pri].worst_cost){
        crossoverWorstToBest(tree[sec].tree, &tree[sec].tree_size, tree[sec].worst_node, tree[sec].best_node,\
                             tree[pri].tree, &tree[pri].tree_size, tree[pri].worst_node, tree[pri].best_node,\
                             offspring[off_var_size].tree, &offspring[off_var_size].tree_size, offspring[off_var_size].tree_index, offspring[off_var_size].postfix, &offspring[off_var_size].postfix_size,\
                             offspring[off_var_size+1].tree, &offspring[off_var_size+1].tree_size, offspring[off_var_size+1].tree_index, offspring[off_var_size+1].postfix, &offspring[off_var_size+1].postfix_size);
      }else{
        crossoverWorstToBest(tree[pri].tree, &tree[pri].tree_size, tree[pri].worst_node, tree[pri].best_node,\
                             tree[sec].tree, &tree[sec].tree_size, tree[sec].worst_node, tree[sec].best_node,\
                             offspring[off_var_size].tree, &offspring[off_var_size].tree_size, offspring[off_var_size].tree_index, offspring[off_var_size].postfix, &offspring[off_var_size].postfix_size,\
                             offspring[off_var_size+1].tree, &offspring[off_var_size+1].tree_size, offspring[off_var_size+1].tree_index, offspring[off_var_size+1].postfix, &offspring[off_var_size+1].postfix_size);
      }
    }
    offspring[off_var_size].tree_cost = -1;
    offspring[off_var_size+1].tree_cost = -1;
    off_var_size = off_var_size + 2;
  }

  (*offspring_size) = off_var_size;
}

void extendMatrix(double **root, int *root_col, double **extend, int *extend_col, int row){
  int rc = (*root_col), i, j;
  int sum_col = (*extend_col) + rc;
  for(i = 0; i < row; i++){
    for(j = rc; j < sum_col; j++){
      root[i][j] = extend[i][j-rc];
    }
  }
  (*root_col) = sum_col;
}

void remove_duplicate(int *index_array, int *size, GPTree *tree){
  int i, j, k, like = 1;
  for(i = 1; i < (*size); i++){
    for(j = i-1; j >= 0; j--){
      if(tree[index_array[i]].tree_size == tree[index_array[j]].tree_size){
        like = 1;
        for(k = 0; k < tree[index_array[i]].tree_size; k++){
          if(strcmp(tree[index_array[i]].tree[k], tree[index_array[j]].tree[k])){
            like = 0;
            break;
          }
        }
        if(like){
          popArray(index_array, size, i);
          i--;
          break;
        }
      }
    }
  }
}

// return combination size
int pf2pfcomb(char **postfix, int postfix_size, char **comb, int thread, int add_dom){
  int comb_size = 0, i, j, u, p, map = 0;
  char str[40];
  spfcomb(postfix, postfix_size, postfix_size-1, comb, &comb_size, thread, add_dom);
  for(i = 0; i < comb_size; i++){
    j = -1;
    u = 0;
    str[0] = 0;
    p = 0;
    while(comb[i][++j] != 0){
      if(comb[i][j] == 'u'){
        u++;
      }else if(comb[i][j] == '*' && u > 0){
        u--;
      }else{
        str[p++] = comb[i][j];
      }
    }
    if(p > 0){
      str[p] = 0;
      sprintf(comb[map++], "%s", str);
    }
  }
  return map;
}

void spfcomb(char **postfix, int postfix_size, int at, char **comb, int *comb_size, int thread, int add_dom){
  int child[2], i, j, k, l, m, n, ccomb_size[] = {0, 0}, f_num = getIntBinary(postfix[at]);
  int dcomb_size = 0, unique, chka, chkb, chkA, chkB, reduce, len;
  child[0] = at-1;
  i = child[0]-1;
  char str[200];
  char function_string[100];
  if(postfix[child[0]][0] == 'f'){
    int leaf = 2;
    while(leaf > 0){
      if(postfix[i][0] == 'f'){
        leaf++;
      }else{
        leaf--;
      }
      i--;
    }
    spfcomb(postfix, postfix_size, child[0], ccomb[thread][at][0], ccomb_size+0, thread, add_dom);
  }else{
    sprintf(ccomb[thread][at][0][0], postfix[child[0]]);
    ccomb_size[0] = 1;
  }
  child[1] = i;
  if(postfix[i][0] == 'f'){
    spfcomb(postfix, postfix_size, child[1], ccomb[thread][at][1], ccomb_size+1, thread, add_dom);
  }else{
    sprintf(ccomb[thread][at][1][0], postfix[child[1]]);
    ccomb_size[1] = 1;
  }
  if(!add_dom){
    if(ccomb_size[0] == 1 && ccomb_size[1] == 1){
      sprintf(function_string, vv_format[f_num]);
    }else if(ccomb_size[0] == 1 || ccomb_size[1] == 1){
      sprintf(function_string, fv_format[f_num]);
    }else{
      sprintf(function_string, ff_format[f_num]);
    }
  }else{
    sprintf(function_string, ff_format[f_num]);
  }
  i = -1, j = -1;
  while(function_string[++i] != 0){
    if(function_string[i] == '+'){
      k = 0;
      while(++j < i){
        str[k] = function_string[j];
        k++;
      }
      str[k] = 0;
      int h = 0;
      if(!strcmp("ab*", str)){
        for(k = 0; k < ccomb_size[0]; k++){
          for(m = 0; m < ccomb_size[1]; m++){
            len = 0;
            len+=sprintf(dcomb[thread][at][dcomb_size]+len, ccomb[thread][at][1][m]);
            len+=sprintf(dcomb[thread][at][dcomb_size]+len, ccomb[thread][at][0][k]);
            len+=sprintf(dcomb[thread][at][dcomb_size]+len, "*");
            dcomb_size++;
          }
        }
      }else{
        if(str[0] == 'u'){
          len = 0;
          len+=sprintf(dcomb[thread][at][dcomb_size]+len, "u");
          dcomb_size++;
        }
        else if(str[0] == 'a'){
          for(m = 0; m < ccomb_size[1]; m++){
            len = 0;
            len+=sprintf(dcomb[thread][at][dcomb_size]+len, ccomb[thread][at][1][m]);
            dcomb_size++;
          }
        }else if(str[0] == 'b'){
          for(k = 0; k < ccomb_size[0]; k++){
            len = 0;
            len+=sprintf(dcomb[thread][at][dcomb_size]+len, ccomb[thread][at][0][k]);
            dcomb_size++;
          }
        }
      }
    }
  }
  (*comb_size) = 0;
  for(i = 0; i < dcomb_size; i++){
    unique = 1;
    for(j = i-1; j >= 0; j--){
      if(!strcmp(dcomb[thread][at][i], dcomb[thread][at][j])){
        unique = 0;
        break;
      }
    }
    if(unique){
      sprintf(comb[(*comb_size)++], dcomb[thread][at][i]);
    }
  }
}

void papernotation(char** postfixstring,int postfixsize, char* infixstring){
  int str_len = 0;
  char** temp_stack = (char**)malloc(postfixsize*OBJECT_ALLOCATE_SIZE);
  int* v_count = (int*)malloc(OBJECT_ALLOCATE_SIZE);
  int temp_top = 0;
  int state = 0;
  int i,j,k;

  for(i = 0; i < postfixsize; i++){
    temp_stack[i] = (char*)malloc(strlen(postfixstring[i]));
    v_count[i] = 0;
    int mul_count = 0;
    int temp_index = 0;
    int len = strlen(postfixstring[i]);
    for(j = 0; j < len; j++){
      if(postfixstring[i][j] == 'v' || postfixstring[i][j] == 'V'){
        if(mul_count == 0){
          mul_count = 1;
        }else{
          temp_stack[i][temp_index] = '*';
          temp_index ++;

        }
        if(postfixstring[i][j] == 'v'){
          temp_stack[i][temp_index] = 'x';
        }else{
          temp_stack[i][temp_index] = 'z';
        }
        v_count[i]++;
        temp_index++;
      }else{
        if(postfixstring[i][j] != '*'){
          temp_stack[i][temp_index] = postfixstring[i][j];
          temp_index ++;
        }
      }
    }
    temp_stack[i][temp_index] = '\0';
  }
  str_len+=sprintf(infixstring+str_len,"y=u+");
  for(i=0; i<postfixsize;i++){
    if(v_count[i]>1){
      str_len+=sprintf(infixstring+str_len,"i");
    }
    for(j=0;j<strlen(temp_stack[i]);j++){
      if(temp_stack[i][j]=='x'){
        str_len+=sprintf(infixstring+str_len,"a");
      }
      if(temp_stack[i][j]=='z'){
        str_len+=sprintf(infixstring+str_len,"d");
      }
    }
    if(v_count[i]==1){
      char* str2 = (char*)malloc(OBJECT_ALLOCATE_SIZE);
      for(k=1;k<strlen(temp_stack[i]);k++){
        str2[k-1] = temp_stack[i][k];
      }
      str2[strlen(temp_stack[i])-1] = '\0';
      str_len+=sprintf(infixstring+str_len,str2);
      free(str2);
    }

    str_len+=sprintf(infixstring+str_len,"(");
    str_len+=sprintf(infixstring+str_len,temp_stack[i]);
    str_len+=sprintf(infixstring+str_len,")");

    if(i<postfixsize-1){
      str_len+=sprintf(infixstring+str_len,"+");
    }
  }
  free(v_count);
  free(temp_stack);
}

void mutation(char** Tree, int* TreeIndex, int *TreeSize, double *cost, char** postfix, int *postfix_size){
  int tree_size = *TreeSize;
  int j, depth = log(tree_size+1)/log(2);

  //mutation node the node that already had a value
  int mutation_index =  randint(0, tree_size);
  while(Tree[mutation_index][0] != 'v' && Tree[mutation_index][0] != 'V' && Tree[mutation_index][0] != 'f'){
    mutation_index = randint(0, tree_size);
  }
  int is_mutation_only_one_node = randint(0,1);
  if (is_mutation_only_one_node){
    if(Tree[mutation_index][0] == 'v'){
      sprintf(Tree[mutation_index], "v%d", randint(0, var_col-1));
    }else if(Tree[mutation_index][0] == 'V'){
      sprintf(Tree[mutation_index], "V%d", randint(0, var_col-1));
    }else{
      if(!SEPARATE_ADD_DOM){
        if(Tree[(2*mutation_index)+1][0] == 'f' && Tree[(2*mutation_index)+2][0] == 'f'){
          sprintf(Tree[mutation_index], "f%d", randint(0, ff-1));
        }else if(Tree[(2*mutation_index)+1][0] == 'f' || Tree[(2*mutation_index)+2][0] == 'f'){
          sprintf(Tree[mutation_index], "f%d", randint(0, fv-1));
        }else{
          sprintf(Tree[mutation_index], "f%d", randint(0, vv-1));
        }
      }
    }
  }else{
    // delete node in subtree from the mutation node
    clear_node_and_itschildren(Tree, mutation_index, tree_size);

    // replace with new subtree at mutation node
    generate_subtree(Tree, mutation_index, tree_size, depth);
  }

  int tree_index_size = 0;
  //change tree index
  for(j=0;j<tree_size;j++){
    if(Tree[j][0] != 0){
      TreeIndex[tree_index_size++] = j;
    }
  }

  //change tree postfix
  parsePostfixBinary(Tree, TreeSize, postfix, postfix_size);

  //set cost to -1
  cost = -1;
}

void clear_node_and_itschildren(char** Tree, int node_index,int tree_size){
  sprintf(Tree[node_index], "\0");
  if((2*node_index)+2 < tree_size){ //its children too
    clear_node_and_itschildren(Tree, (2*node_index)+1 ,tree_size);
    clear_node_and_itschildren(Tree, (2*node_index)+2 ,tree_size);
  }
}

void generate_subtree(char** Tree, int node_index, int TreeSize, int depth){
  int is_function;
  int randnum;

  //random to replace this node with terminal or function
  if(node_index == 0){
    is_function = 1;
  }else{
    if((2*node_index)+2 > powInt(2, depth) - 1){
      is_function = 0;
    }else{
      is_function =  randint(0, 1);
    }
  }

  //replace node with choosen type of the node
  if(is_function == 1){
    generate_subtree(Tree, (2*node_index)+1, TreeSize, depth);
    generate_subtree(Tree, (2*node_index)+2, TreeSize, depth);
    if(!SEPARATE_ADD_DOM){
      if(Tree[(2*node_index)+1][0] == 'f' && Tree[(2*node_index)+2][0] == 'f'){
        sprintf(Tree[node_index], "f%d", randint(0, ff-1));
      }else if(Tree[(2*node_index)+1][0] == 'f' || Tree[(2*node_index)+2][0] == 'f'){
        sprintf(Tree[node_index], "f%d", randint(0, fv-1));
      }else{
        sprintf(Tree[node_index], "f%d", randint(0, vv-1));
      }
    }else{
      sprintf(Tree[node_index], "f0");
    }
  }else{
    randnum = randint(0, var_col-1);
    sprintf(Tree[node_index], "v%d", randnum);
  }
  //not have to free char* s
}

void printRecentUsageMem(char *prefix){
  long cmp = compareRecentUsageMem();
  if(cmp != 0){
    printf("---%s%ld\n", prefix, cmp);
  }
}

#endif
