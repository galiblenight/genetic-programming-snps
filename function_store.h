#ifndef FUNC_STORE
#define FUNC_STORE
  #include <math.h>
  #include "essential.h"

  extern void sigmoid_mat(double **matrix, int row, int col, double **result){
    int i, j;
    for(i = 0; i < row; i++){
      for(j = 0; j < col; j++){
        result[i][j] = 1.0/(1.0 + exp(-matrix[i][j]));
      }
    }
  }

#endif
