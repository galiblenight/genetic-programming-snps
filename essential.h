#ifndef _GenP_Essential
#define _GenP_Essential
  #include <math.h>
  #include <stdio.h>
  #include <sys/time.h>
  #include <stdlib.h>
  #include <unistd.h>
  #include "init.c"
  #include <string.h>

  extern int getTreeIndexFromPostfixIndex(char **tree, int depth, int postfix_index){
    int start = powInt(2, depth-1)-1, pointer = start, nextleaf = start, index = 0, i = -1;
    while(1){
      if(tree[pointer][0] != 0){
        if(index == postfix_index){
          return pointer;
        }
        index++;
      }
      if(pointer >= start){
        if(pointer%2 == 1){
          pointer += 1;
        }else{
          nextleaf += 2;
          pointer = (pointer-2)/2;
        }
      }else{
        if(pointer%2 == 1){
          pointer = nextleaf;
        }else{
          pointer = pointer = (pointer-2)/2;
        }
      }
    }
  }

  /* get power of interger */
  extern int powInt(int base, int power){
    int result = 1;
    while(--power >= 0)
      result = result * base;
    return result;
  }

  extern int getIntBinary(char *str){
    int pointer = strlen(str), result = 0, level = 0;
    while((int)str[--pointer] >= 48 && (int)str[pointer] <= 57){
      result += ((int)str[pointer]-48)*powInt(10, level);
      level++;
    }
    return result;
  }

  extern long powLong(long base, long power){
    long result = 1;
    while(--power >= 0)
      result = result * base;
    return result;
  }

  /* get string length of integer */
  extern int tostrlen(int value){
    int i = 0;
    while(powInt(10, ++i) < value);
    return i;
  }

  /* get integer from char */
  extern int ctoi(char ch){
    return ch-(int)'0';
  }

  /* random integer in range min and max */
  extern int randint(int min, int max){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    srand(tv.tv_sec+tv.tv_usec);
    if(max-min <= 0){
      return min;
    }else
      return min + (rand()%(max-min+1));
  }

  extern double randrange(double min, double max){
    struct timeval tv;
    gettimeofday(&tv, NULL);
    srand(tv.tv_sec+tv.tv_usec);
    if(max-min <= 0){
      return min;
    }else
      return min + ((max-min)*((double)(rand()%100000000)/100000000));
  }

  /* print structure of array */
  extern void printa(int* array, int size){
    int i;
    printf("[");
    for(i = 0; i < size; i++){
      printf("%d", array[i]);
      if(i < size-1){
        printf(", ");
      }
    }
    printf("]\n");
  }

  /* remove 1 object in array */
  extern int popArray(int* array, int* arraySize, int index){
    int i, pop;
    pop = array[index];
    for(i = index; i < *arraySize; i++){
      array[i] = array[i+1];
    }
    (*arraySize)--;
    return pop;
  }

  /* add 1 integer to tail of an array */
  extern void appendArray(int* array, int* arraySize, int value){
    array[(*arraySize)] = value;
    (*arraySize)++;
  }

  /* find index of interger in array */
  extern int indexOf(int* array, int* arraySize, int value){
    int i, s = (*arraySize);
    for(i = 0; i < s; i++){
      if(array[i] == value)
        return i;
    }
    return -1;
  }

  extern void splitTerminal(char *terminal, int *term, int *depth){
    (*term) = 0;
    (*depth) = 0;
    int lensf = strlen(terminal), *combine[4] = {depth, term}, i, j = 0, k = 0;
    for(i = lensf-1; i > 0; i--){
      char ch = terminal[i];
      if(ch == '|'){
        k = 0;
        j++;
      }else{
        (*(combine[j])) += ctoi(ch) * powInt(10, k++);
      }
    }
  }

  extern void getFunctionPattern(char *str, int randnum, int depth, int current_index, int childs){
    sprintf(str, "f%d|%d/", randnum, depth);
    int last = current_index + childs, i;
    for(i = current_index; i < last; i++){
      sprintf(str, "%s|%d", str, i);
    }
  }

  extern void splitFunction(char *function, int *func, int *depth, int *consider, int *con_size){
    (*func) = 0;
    (*depth) = 0;
    int lensf = strlen(function), *combine[2] = {depth, func}, i, j = 0, k = 0, l = 0, mode = 0;
    for(i = lensf-1; i > 0; i--){
      char ch = function[i];
      if(ch == '/'){
        mode = 1;
        j = 0;
      }else if(ch == '|'){
        k = 0;
        j++;
        if(mode == 0){
          appendArray(consider, con_size, l);
          l = 0;
        }
      }else if(mode == 1){
        (*(combine[j])) += ctoi(ch) * powInt(10, k++);
      }else{
        l += ctoi(ch) * powInt(10, k++);
      }
    }
  }

  extern void getRandomFuncCustom(int depth, int max_depth, int max_func_set_size, int flist_size\
                                    , int *func_child_num_index, int *func_child_num_list\
                                    , int *stack_child_func, long *function_remain\
                                    , int *randnum, int *size_of_rand){
    int less_child_function = 1, index = 0;
    int i, j, fr = (*function_remain);
    for(i = 0; i < flist_size-1; i++){
      if(fr - powLong(func_child_num_index[flist_size-1] - func_child_num_index[i], (max_depth - depth)) >= 0)
        break;
    }
    int rn = randint(stack_child_func[i], max_func_set_size-1);
    (*randnum) = rn;
    for(i = 0; i < flist_size-1; i++){
      if(stack_child_func[i+1] > rn)
        break;
    }
    // printa(func_child_num_index, flist_size);
    (*size_of_rand) = func_child_num_index[i];
    (*function_remain) -= powLong(func_child_num_index[flist_size-1] - func_child_num_index[i-1], (max_depth - depth));
  }

  /* generate random number of binary tree and avoid it generate uni-function too much */
  extern int getRandomFuncBinary(int uni_term, int func_set_size, int *function_remain, int index, int max_depth){
    int depth = (int)(log(index+1)/log(2));
    if((*function_remain) - powInt(2, (max_depth - depth - 2)) < 0){
      int randnum = randint(uni_term+1, func_set_size-1);
      return randnum;
    }else{
      int randnum = randint(0, func_set_size-1);
      if(randnum <= uni_term){
        (*function_remain) -= powInt(2, (max_depth - depth - 2));
      }
      return randnum;
    }
  }

  extern void transpost(double **matrix, int row, int col, double **result){
    int i, j;
    for(i = 0; i < col; i++){
      for(j = 0; j < row; j++){
        result[i][j] = matrix[j][i];
      }
    }
  }

  void prints(char **args, int size){
    int i;
    printf("[");
    for(i = -1; ++i < size; printf("\"%s\", ", args[i]));
    printf("\b\b]\n");
  }

  extern double is_singular(double **matrix, int fromx, int fromy, int size, double ***matrix_dummy){
    int i, j, k, l, s = size, pointer = 0;
    long double res = 0;
    for(i = 0; i < size; i++){
      for(j = 0; j < size; j++){
        matrix_dummy[0][i][j] = matrix[fromy + i][fromx + j];
      }
    }
    while(--s > 0){
      if(isinf(1.0/matrix_dummy[pointer][0][0])){
        s++;
        k = -1;
        while(++k < s){
          if(matrix_dummy[pointer][k][0] != 0)
            break;
        }
        if(k == s)
          return 0;
        for(i = 0; i < s; i++){
          matrix_dummy[pointer][0][i] -= matrix_dummy[pointer][k][i];
        }
        s--;
      }
      double a11 = matrix_dummy[pointer][0][0];
      for(i = 0; i < s; i++){
        for(j = 0; j < s; j++){
          matrix_dummy[!pointer][i][j] = a11 * matrix_dummy[pointer][i + 1][j + 1] - matrix_dummy[pointer][0][j + 1] * matrix_dummy[pointer][i + 1][0];
        }
      }
      pointer = !pointer;
    }
    res = matrix_dummy[pointer][0][0];
    return res;
  }

  // use chio's condensation method
  // extern double det(double **matrix, int fromx, int fromy, int size){
  //   int i, j, k, l, s = size, pointer = 0;
  //   double res = 0, stack = 1;
  //   if(matrix_dummy_var_size < size){
  //     matrix_dummy = malloc(2*sizeof(double)*size*size);
  //     matrix_dummy[0] = malloc(sizeof(double)*size*size);
  //     matrix_dummy[1] = malloc(sizeof(double)*size*size);
  //     for(i = 0; i < size; i++){
  //       matrix_dummy[0][i] = malloc(sizeof(double)*size);
  //       matrix_dummy[1][i] = malloc(sizeof(double)*size);
  //     }
  //     matrix_dummy_var_size = size;
  //   }
  //   for(i = 0; i < size; i++){
  //     matrix_dummy[0][i] = malloc(sizeof(double)*size);
  //     for(j = 0; j < size; j++){
  //       matrix_dummy[0][i][j] = matrix[fromy + i][fromx + j];
  //     }
  //   }
  //   while(--s > 0){
  //     if(isinf(1.0/matrix_dummy[pointer][0][0])){
  //       s++;
  //       k = -1;
  //       while(++k < s){
  //         if(matrix_dummy[pointer][k][0] != 0)
  //           break;
  //       }
  //       if(k == s)
  //         return 0;
  //       for(i = 0; i < s; i++){
  //         matrix_dummy[pointer][0][i] -= matrix_dummy[pointer][k][i];
  //       }
  //       s--;
  //     }
  //     double a11 = matrix_dummy[pointer][0][0];
  //     stack /= pow(a11, s - 1);
  //     for(i = 0; i < s; i++){
  //       for(j = 0; j < s; j++){
  //         matrix_dummy[!pointer][i][j] = a11 * matrix_dummy[pointer][i + 1][j + 1] - matrix_dummy[pointer][0][j + 1] * matrix_dummy[pointer][i + 1][0];
  //       }
  //     }
  //     pointer = !pointer;
  //   }
  //   double ans = matrix_dummy[pointer][0][0];
  //   return stack * ans;
  // }

  extern int inverseNxN(double **matrix, int size, double **result, double ***matrix_dummy){
    int i, j, k, l;
    double m, n;
    if(isinf(1.0/is_singular(matrix, 0, 0, size, matrix_dummy))){
      return 0;
    }
    for(i = 0; i < size; i++){
      for(j = 0; j < size; j++){
        matrix_dummy[0][i][j] = matrix[i][j];
      }
    }
    for(i = 0; i < size; i++){
      for(j = 0; j < size; j++){
        result[i][j] = (i==j);
      }
    }
    for(i = 0; i < size; i++){
      if(matrix_dummy[0][i][i] == 0.0){
        for(k = i; k < size; k++){
          if(matrix_dummy[0][k][i] != 0.0)
            break;
        }
        if(k == size){
          return 0;
        }
        for(l = 0; l < size; l++){
          n = matrix_dummy[0][i][l];
          matrix_dummy[0][i][l] = matrix_dummy[0][k][l];
          matrix_dummy[0][k][l] = n;
          n = result[i][l];
          result[i][l] = result[k][l];
          result[k][l] = n;
        }
      }
      if(matrix_dummy[0][i][i] != 1.0){
        m = matrix_dummy[0][i][i];
        for(l = 0; l < size; l++){
          matrix_dummy[0][i][l] /= m;
          result[i][l] /= m;
        }
      }
      for(j = 0; j < size; j++){
        if(i != j){
          m = matrix_dummy[0][j][i];
          for(k = 0; k < size; k++){
            matrix_dummy[0][j][k] -= matrix_dummy[0][i][k]*m;
            result[j][k] -= result[i][k]*m;
          }
        }
      }
    }
    return 1;
  }

  extern int inverse3x3(double **matrix, double **result){
    int i, j;
    double m[3][3], det = 0;
    for(i = 0; i < 3; i++){
      for(j = 0; j < 3; j++){
        int si = i==0, pi = i==1, sj = j==0, pj = j==1;
        m[i][j] = matrix[sj][si]*matrix[sj+1+pj][si+1+pi] - matrix[sj+1+pj][si]*matrix[sj][si+1+pi];
        if(i == 0){
          det += ((j==1)?-1:1) * matrix[0][j] * (matrix[1][sj]*matrix[2][sj+1+pj] - matrix[2][sj]*matrix[1][sj+1+pj]);
        }
      }
    }
    det = 1/det;
    if(isinf(det) || isnan(det)){
      return 0;
    }
    for(i = 0; i < 3; i++){
      for(j = 0; j < 3; j++){
        result[i][j] = det * m[i][j] * ((i+j)%2==1?-1:1);
      }
    }
    return 1;
  }

  extern int inverse2x2(double **matrix, double **result){
    double det = 1.0/((matrix[0][0] * matrix[1][1]) - (matrix[0][1] * matrix[1][0]));
    if(isinf(det) || isnan(det)){
      return 0;
    }
    result[0][0] = det * matrix[1][1];
    result[0][1] = -det * matrix[0][1];
    result[1][0] = -det * matrix[1][0];
    result[1][1] = det * matrix[0][0];
    return 1;
  }

  extern void diag(double **input_matrix, int size, double **result){
    int i, j;
    for(i = 0; i < size; i++){
      for(j = 0; j < size; j++){
        if(i == j)
          result[i][j] = input_matrix[i][0];
        else
          result[i][j] = 0.0;
      }
    }
  }

  extern void ones(int row, int col, double **result){
    int i, j;
    for(i = 0; i < row; i++){
      for(j = 0; j < col; j++){
        result[i][j] = 1.0;
      }
    }
  }

  extern void minus_mat(double** m1, double** m2, int row, int col, double** res){
    int i, j;
    for(i = 0; i < row; i++){
      for(j = 0; j < col; j++){
        res[i][j] = m1[i][j] - m2[i][j];
      }
    }
  }

  extern int xmatrix(double** m1, int m1r, int m1c, double** m2, int m2r, int m2c, double** res){
    if(m1c != m2r){
      return 0;
    }
    int i, j, k, l;
    for(i = 0; i < m1r; i++){
      for(j = 0; j < m2c; j++){
        res[i][j] = 0;
        for(k = 0; k < m1c; k++){
          res[i][j] += m1[i][k] * m2[k][j];
        }
      }
    }
    return 1;
  }

  void replaceStr(char *root, char *element, char *replace){
    int i = -1, j, k, l;
    char str1[strlen(root)], str2[strlen(root)];
    while(root[++i] != 0){
      if(root[i] == element[0]){
        j = 0;
        while (root[i] == element[j]) {
          if(element[j+1] == 0){
            l = i - j;
            for(k = 0; k < l; k++){
              str1[k] = root[k];
            }
            str1[k] = 0;
            k = i;
            l = 0;
            while(root[++k] != 0){
              str2[l] = root[k];
              l++;
            }
            str2[l] = 0;
            sprintf(root, "%s%s%s", str1, replace, str2);
            break;
          }else if(root[i+1] == 0){
            break;
          }
          i++;
          j++;
        }
      }
    }
  }

  void fontColor(char *color){
    if(!strcmp(color, "RED")){
      printf("\x1B[31m");
    }else if(!strcmp(color, "MAGENTA")){
      printf("\x1B[35m");
    }else if(!strcmp(color, "NORMAL")){
      printf("\x1B[0m");
    }else if(!strcmp(color, "GREEN")){
      printf("\x1B[32m");
    }else if(!strcmp(color, "YELLOW")){
      printf("\x1B[33m");
    }else if(!strcmp(color, "CYAN")){
      printf("\x1B[36m");
    }else if(!strcmp(color, "WHITE")){
      printf("\x1B[37m");
    }
  }

  int chkBadPostfix(char **postfix, int postfix_size){
    int i = 0, v = 0, f = 0;
    for(;i < postfix_size; i++){
      // printf("%s ", postfix[i]);
      if(postfix[i][0] == 'v' || postfix[i][0] == 'V'){
        v++;
      }else if(postfix[i][0] == 'f'){
        f++;
      }
    }
    // printf("\n");
    return (f == v-1 && v > 1);
  }

  void printTree(char **args, int size, char *fcolor, char *vcolor, char *_color){
    int i;
    printf("[ ");
    for(i = 0; i < size; i++){
      if(args[i][0] != 0){
        if(args[i][0] == 'f'){
          fontColor(fcolor);
        }if(args[i][0] == 'v' || args[i][0] == 'V'){
          fontColor(vcolor);
        }
        printf("%s ", args[i]);
      }else{
        fontColor(_color);
        printf("_ ");
      }
    }
    fontColor("NORMAL");
    printf("]");
  }

  void printTreeln(char **args, int size, char *fcolor, char *vcolor, char *_color){
    printTree(args, size, fcolor, vcolor, _color);
    printf("\n");
  }

  int indexStrOf(char *root, char *element){
    int i = -1, j, k, l;
    while(root[++i] != 0){
      if(root[i] == element[0]){
        j = 0;
        while (root[i] == element[j]) {
          if(element[j+1] == 0){
            return i-j;
          }else if(root[i+1] == 0){
            break;
          }
          i++;
          j++;
        }
      }
    }
    return -1;
  }

#endif
