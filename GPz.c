#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <signal.h>
#include <time.h>
#include <unistd.h>
#include "setting.c"

// TODO now this program goes infinite loop because don't have remove duplicated function

#if defined (VARIABLE_COST_ESTIMATE_TIMES) && defined (VARIABLE_ALLOCATE_SIZE)\
    && (defined OBJECT_ALLOCATE_SIZE) && defined (INPUT_FILE) && defined (EPORCH)\
    && (defined LEAF_COST) && (defined POPULATION_SIZE) && (defined BLUK_OBJECT_ALLOCATE_SIZE)\
    && (defined C_MULT) && (defined GENERATION) && (defined PASS_PARENT_SIZE)\
    && (defined MAXIMUM_RANDOM_TREE_DEPTH) && (defined COST_FUNCTION) && (defined NUM_THREADS)\

#include "GenP.c"

GPTree **Tree;
double *tree_cost;
int progress, gen, indiv_size;
void operation(GPTree**, int);
void *t_operation(void*);
char* vvset[] = {"a","A","aA*","b","B","bB*","ab*","Ab*","aB*","AB*"};
char* fvset[] = {"a","A","aA*","b","ab*","Ab*"};
char* ffset[] = {"a","b", "ab*"};
int ffset_size = 3, fvset_size = 6, vvset_size = 10, ff_func_set_size, fv_func_set_size, vv_func_set_size;
long count = 0;

int usage = 0;
char *remove_ext (char* mystr, char dot, char sep) {
    char *retstr, *lastdot, *lastsep;

    // Error checks and allocate string.

    if (mystr == NULL)
        return NULL;
    if ((retstr = malloc (strlen (mystr) + 1)) == NULL)
        return NULL;

    // Make a copy and find the relevant characters.

    strcpy (retstr, mystr);
    lastdot = strrchr (retstr, dot);
    lastsep = (sep == 0) ? NULL : strrchr (retstr, sep);

    // If it has an extension separator.

    if (lastdot != NULL) {
        // and it's before the extenstion separator.

        if (lastsep != NULL) {
            if (lastsep < lastdot) {
                // then remove it.

                *lastdot = '\0';
            }
        } else {
            // Has extension separator with no path separator.

            *lastdot = '\0';
        }
    }

    // Return the modified string.

    return retstr;
}//http://stackoverflow.com/questions/2736753/how-to-remove-extension-from-file-name

int main(int argc, char **argv){
//************************start logging************************************//
  struct tm *tm;
  time_t t;
  char str_time[100];
  t = time(NULL);
  tm = localtime(&t);
  strftime(str_time, sizeof(str_time), "%Y-%m-%d-%H:%M:%S", tm);
  FILE *logging_file;
  char* oldtreestr = (char*)malloc(OBJECT_ALLOCATE_SIZE);
  char* filename = (char*)malloc(OBJECT_ALLOCATE_SIZE);
  int oldtreestrlen = 0;
  int filenamelen = 0;
  filenamelen+=sprintf(filename+filenamelen,"log/log_");
  filenamelen+=sprintf(filename+filenamelen,remove_ext(basename(INPUT_FILE), '.', '/'));
  filenamelen+=sprintf(filename+filenamelen,"_");
  filenamelen+=sprintf(filename+filenamelen,str_time);
  filenamelen+=sprintf(filename+filenamelen,".csv\0");
  oldtreestrlen+=sprintf(oldtreestr+oldtreestrlen,"%d",0);
  double acc_ = 0.0;
//***********************************************************************//

  startTime();
  init();
  int maximum_func = (powInt(2, MAXIMUM_RANDOM_TREE_DEPTH)/2)-1;
  int i, j, rnd;
  Tree = malloc(2*BLUK_OBJECT_ALLOCATE_SIZE);
  int *selected = malloc(OBJECT_ALLOCATE_SIZE), selected_size = 0;
  int *index_array = malloc(OBJECT_ALLOCATE_SIZE);
  int *sorted_selected = malloc(OBJECT_ALLOCATE_SIZE);
  tree_cost = malloc(OBJECT_ALLOCATE_SIZE);
  double *selected_cost = malloc(OBJECT_ALLOCATE_SIZE);
  indiv_size = POPULATION_SIZE;
  char* paper_notation = (char*)malloc(OBJECT_ALLOCATE_SIZE);
  int comb_size, rand_mu, parent_mutate, pm;
  char* input_file = malloc(100);
  if(argv[1] != NULL){
    readdatafile(argv[1]);
  }else{
    readdatafile(INPUT_FILE);
  }
  ff_func_set_size = sigmoid_function_generate(ffset, ffset_size, "ff");
  fv_func_set_size = sigmoid_function_generate(fvset, fvset_size, "fv");
  vv_func_set_size = sigmoid_function_generate(vvset, vvset_size, "vv");
  for(gen = 0; gen < 2; gen++){
    Tree[gen] = malloc(BLUK_OBJECT_ALLOCATE_SIZE);
    for(i = 0; i < 3*POPULATION_SIZE; i++){
      Tree[gen][i].tree = malloc(OBJECT_ALLOCATE_SIZE);
      for(j = 0; j < 100; j++){
        Tree[gen][i].tree[j] = malloc(10);
      }
      Tree[gen][i].cost = malloc(OBJECT_ALLOCATE_SIZE);
      Tree[gen][i].postfix = malloc(OBJECT_ALLOCATE_SIZE);
      for(j = 0; j < 100; j++){
        Tree[gen][i].postfix[j] = malloc(10);
      }
      Tree[gen][i].tree_index = malloc(OBJECT_ALLOCATE_SIZE);
    }
  }

  // // test
  // char **a = malloc(1000);
  // int kk, cs;
  // for(kk = 0; kk < 15; kk++){
  //   a[kk] = malloc(20);
  // }
  // sprintf(a[0], "v1");
  // sprintf(a[1], "v2");
  // sprintf(a[2], "f4");
  // sprintf(a[3], "v3");
  // sprintf(a[4], "v4");
  // sprintf(a[5], "f4");
  // sprintf(a[6], "f4");
  // sprintf(a[7], "v5");
  // sprintf(a[8], "v6");
  // sprintf(a[9], "f4");
  // sprintf(a[10], "v7");
  // sprintf(a[11], "v8");
  // sprintf(a[12], "f4");
  // sprintf(a[13], "f4");
  // sprintf(a[14], "f4");
  // cs = pf2pfcomb(a, 15, comb[0], 0, SEPARATE_ADD_DOM);
  // //
  // for(i = 0; i < cs; i++){
  //   printf("%s ", comb[0][i]);
  // }
  // return 0;

  // create first individual
  for(i = 0; i < POPULATION_SIZE; i++){
    randomStrAsymBinary(5, 0, getVariableCol(), 0, randint(1, maximum_func), MAXIMUM_RANDOM_TREE_DEPTH,\
                        0, Tree[0][i].tree, &Tree[0][i].tree_size, Tree[0][i].postfix, &Tree[0][i].postfix_size,\
                        Tree[0][i].tree_index);
    Tree[0][i].tree_cost = -1;
  }
  gen = 1;
  // start genetic programming
  for(rnd = 0; rnd < GENERATION; rnd++){ // --------------
    gen = !gen;
    for(i = 0; i < indiv_size; i++){
      fitTreeSize(Tree[gen][i].tree, &Tree[gen][i].tree_size);
    }
    operation(Tree, indiv_size);

    sort_by_error(tree_cost, index_array, indiv_size);

    for(i = indiv_size-1; i >= 0; i--){
      if(tree_cost[index_array[i]] < 0 || Tree[gen][index_array[i]].tree_size == 0){
        popArray(index_array, &indiv_size, i);
      }
    }
    remove_duplicate(index_array, &indiv_size, Tree[gen]);
    for(i = 0; i < PASS_PARENT_SIZE; i++){
      sorted_selected[i] = index_array[i];
    }
    stochastic(index_array, tree_cost, indiv_size, selected, &selected_size);
    crossover_bestfit(Tree[gen], selected, &selected_size, Tree[!gen], &indiv_size);
    for(i = 0; i < indiv_size; i++){
      rand_mu = randint(1, 100);
      if(rand_mu <= PERCENT_MUTATION){
        mutation(Tree[!gen][i].tree, Tree[!gen][i].tree_index, &Tree[!gen][i].tree_size, &Tree[!gen][i].tree_cost, Tree[!gen][i].postfix, &Tree[!gen][i].postfix_size);
      }
    }
    parent_mutate = 0;
    for(i = 0; i < PASS_PARENT_SIZE; i++){
      if(i == 0){
        printf("Accuracy : %lf\n", Tree[gen][sorted_selected[0]].Accuracy);
        acc_ = Tree[gen][sorted_selected[0]].Accuracy;
      }
    PAMUTSTR:
      for(j = 0; j < Tree[gen][sorted_selected[i]].tree_size; j++){
        sprintf(Tree[!gen][indiv_size].tree[j], "%s", Tree[gen][sorted_selected[i]].tree[j]);
      }
      Tree[!gen][indiv_size].tree_size = Tree[gen][sorted_selected[i]].tree_size;
      for(j = 0; j < Tree[gen][sorted_selected[i]].postfix_size; j++){
        sprintf(Tree[!gen][indiv_size].postfix[j], "%s", Tree[gen][sorted_selected[i]].postfix[j]);
        Tree[!gen][indiv_size].tree_index[j] = Tree[gen][sorted_selected[i]].tree_index[j];
      }
      Tree[!gen][indiv_size].postfix_size = Tree[gen][sorted_selected[i]].postfix_size;
      Tree[!gen][indiv_size].best_node = Tree[gen][sorted_selected[i]].best_node;
      Tree[!gen][indiv_size].worst_node = Tree[gen][sorted_selected[i]].worst_node;
      Tree[!gen][indiv_size].best_cost = Tree[gen][sorted_selected[i]].best_cost;
      Tree[!gen][indiv_size].worst_cost = Tree[gen][sorted_selected[i]].worst_cost;
      for(j = 0; j < Tree[gen][sorted_selected[i]].postfix_size; j++){
        Tree[!gen][indiv_size].cost[j] = Tree[gen][sorted_selected[i]].cost[j];
        Tree[!gen][indiv_size].Accuracy = Tree[gen][sorted_selected[i]].Accuracy;
      }
      Tree[!gen][indiv_size].tree_cost = Tree[gen][sorted_selected[i]].tree_cost;
      indiv_size++;
      if(parent_mutate)goto PAMUTEND;
    }
    parent_mutate = 1;
    i = 0;
    for(pm = 0; pm < NUM_BEST_PARENT_MUTATED; pm++){
      goto PAMUTSTR;
      PAMUTEND:
        mutation(Tree[!gen][indiv_size-1].tree, Tree[!gen][indiv_size-1].tree_index, &Tree[!gen][indiv_size-1].tree_size, &Tree[!gen][indiv_size-1].tree_cost, Tree[!gen][indiv_size-1].postfix, &Tree[!gen][indiv_size-1].postfix_size);
        Tree[!gen][indiv_size-1].tree_cost = -1;
    }

    for(; indiv_size < POPULATION_SIZE; indiv_size++){
      randomStrAsymBinary(5, 0, getVariableCol(), 0, randint(1, maximum_func), MAXIMUM_RANDOM_TREE_DEPTH, 0, Tree[!gen][indiv_size].tree,\
         &Tree[!gen][indiv_size].tree_size, Tree[!gen][indiv_size].postfix, &Tree[!gen][indiv_size].postfix_size, Tree[!gen][indiv_size].tree_index);
      Tree[!gen][indiv_size].tree_cost = -1;
    }
    if(indiv_size > POPULATION_SIZE*2){
      indiv_size = POPULATION_SIZE*2;
    }
    printf("size: %d\n", indiv_size);
    printTree(Tree[gen][sorted_selected[0]].tree, Tree[gen][sorted_selected[0]].tree_size, "CYAN", "GREEN", "RED");
    comb_size = pf2pfcomb(Tree[gen][sorted_selected[0]].postfix, Tree[gen][sorted_selected[0]].postfix_size, comb[0], 0, SEPARATE_ADD_DOM);
    for(i = 0; i < comb_size; i++){
      printf("%s ", comb[0][i]);
    }
    printf("\n");
    papernotation(comb[0], comb_size, paper_notation);
    printf("Notation : %s\n", paper_notation);
    printf("Best cost : %lf\n", tree_cost[sorted_selected[0]]);

    //************************for logging************************************//
    char* treestr = (char*)malloc(OBJECT_ALLOCATE_SIZE);
    int treestrlen = 0;
    for(i = 0; i < Tree[gen][sorted_selected[0]].tree_size; i++){
        treestrlen+=sprintf(treestr+treestrlen,"%s ", Tree[gen][sorted_selected[0]].tree[i]);
    }
    if(strcmp(oldtreestr ,treestr)||rnd==GENERATION-1){
      logging_file = fopen(filename, "a");
      long atime = endTime();
      fprintf(logging_file, "%d,%ld.%ld,%d,%lf,%lf,%s,%s\n",rnd,(long)(atime/1000000), (long)(atime%1000000) ,indiv_size,tree_cost[sorted_selected[0]],acc_,treestr, paper_notation);
      free(oldtreestr);
      oldtreestr = (char*)malloc(OBJECT_ALLOCATE_SIZE);
      oldtreestrlen = 0;
      oldtreestrlen+=sprintf(oldtreestr+oldtreestrlen,treestr);
      fclose(logging_file);
    }
    free(treestr);
  //***********************************************************************//
    printf("GENERATION : %d\n\n", rnd+1);
    // printf("---------------- v1 ------------------------\n");
  } // ---------------------------------------------------
  FILE *logging_main;
  if( access( "log/main_log.csv", F_OK ) == -1 ) {
    // file doesn't exist
    printf("file doesn't exist\n");
    logging_main = fopen("log/main_log.csv", "a");
    fprintf(logging_main, "LOG_FILE,Start_Time,Finish_Time,Duration,Generation,Indiv_Size,Best_Cost,Accuracy,BestTree_Infix,Expression,");
    fprintf(logging_main, "TRUE_POSITIVE,");
    fprintf(logging_main, "FALSE_POSITIVE,");
    fprintf(logging_main, "VARIABLE_COST_ESTIMATE_TIMES,");
    fprintf(logging_main, "VARIABLE_ALLOCATE_SIZE,");
    fprintf(logging_main, "BLUK_OBJECT_ALLOCATE_SIZE,");
    fprintf(logging_main, "OBJECT_ALLOCATE_SIZE,");
    fprintf(logging_main, "COMB_ALLOCATE_SIZE,");
    fprintf(logging_main, "COMB_OBJECT_ALLOCATE_SIZE,");
    fprintf(logging_main, "NUM_COMB_OBJECT,");
    fprintf(logging_main, "NUM_COMB_OBJECT_ALLOCATE,");
    fprintf(logging_main, "INPUT_FILE,");
    fprintf(logging_main, "EPORCH,");
    fprintf(logging_main, "GENERATION,");
    fprintf(logging_main, "LEAF_COST,");
    fprintf(logging_main, "POPULATION_SIZE,");
    fprintf(logging_main, "C_MULT,");
    fprintf(logging_main, "PASS_PARENT_SIZE,");
    fprintf(logging_main, "MAXIMUM_RANDOM_TREE_DEPTH,");
    fprintf(logging_main, "COST_FUNCTION,");
    fprintf(logging_main, "NUM_THREADS,");
    fprintf(logging_main, "PERCENT_MUTATION,");
    fprintf(logging_main, "NUM_BEST_PARENT_MUTATED,");
    fprintf(logging_main, "SEPARATE_ADD_DOM,");
    fprintf(logging_main, "\n");
    fclose(logging_main);
  }
  logging_main = fopen("log/main_log.csv", "a");
  char str_time2[100];
  t = time(NULL);
  tm = localtime(&t);
  strftime(str_time2, sizeof(str_time2), "%Y-%m-%d-%H:%M:%S", tm);
  char* treestr = (char*)malloc(OBJECT_ALLOCATE_SIZE);
  int treestrlen = 0;
  for(i = 0; i < Tree[gen][sorted_selected[0]].tree_size; i++){
    treestrlen+=sprintf(treestr+treestrlen,"%s ", Tree[gen][sorted_selected[0]].tree[i]);
  }
  long atime = endTime();
  printf("$$ %d %d\n", Tree[gen][sorted_selected[0]].True_positive, Tree[gen][sorted_selected[0]].False_positive);
  fprintf(logging_main, "%s,%s,%s,%ld.%ld,%d,%d,%lf,%lf,%s,%s,",filename,str_time,str_time2,(long)(atime/1000000), (long)(atime%1000000),rnd ,indiv_size,tree_cost[sorted_selected[0]],acc_,treestr, paper_notation);
  fprintf(logging_main, "%d,",Tree[gen][sorted_selected[0]].True_positive);
  fprintf(logging_main, "%d,",Tree[gen][sorted_selected[0]].False_positive);
  fprintf(logging_main, "%d,",VARIABLE_COST_ESTIMATE_TIMES);
  fprintf(logging_main, "%d,",VARIABLE_ALLOCATE_SIZE);
  fprintf(logging_main, "%d,",BLUK_OBJECT_ALLOCATE_SIZE);
  fprintf(logging_main, "%d,",OBJECT_ALLOCATE_SIZE);
  fprintf(logging_main, "%d,",COMB_ALLOCATE_SIZE);
  fprintf(logging_main, "%d,",COMB_OBJECT_ALLOCATE_SIZE);
  fprintf(logging_main, "%d,",NUM_COMB_OBJECT);
  fprintf(logging_main, "%d,",NUM_COMB_OBJECT_ALLOCATE);
  fprintf(logging_main, "%s,",INPUT_FILE);
  fprintf(logging_main, "%d,",EPORCH);
  fprintf(logging_main, "%d,",GENERATION);
  fprintf(logging_main, "%d,",LEAF_COST);
  fprintf(logging_main, "%d,",POPULATION_SIZE);
  fprintf(logging_main, "%lf,",C_MULT);
  fprintf(logging_main, "%d,",PASS_PARENT_SIZE);
  fprintf(logging_main, "%d,",MAXIMUM_RANDOM_TREE_DEPTH);
  if(COST_FUNCTION == BIC){
    fprintf(logging_main, "BIC,");
  }else{
    fprintf(logging_main, "AIC,");
  }
  fprintf(logging_main, "%d,",NUM_THREADS);
  fprintf(logging_main, "%d,",PERCENT_MUTATION);
  fprintf(logging_main, "%d,",NUM_BEST_PARENT_MUTATED);
  fprintf(logging_main, "%d,",SEPARATE_ADD_DOM);
  fprintf(logging_main, "\n");
  fclose(logging_main);
  long end_time = endTime();
  printf("Finish. duration %ld.%ld sec.\n", (long)(end_time/1000000), (long)(end_time%1000000));
  return 0;
}

void *t_operation(void *th){
  while(progress < indiv_size){
    int i = progress++, j, cost_index;
    double cost;
    if(Tree[gen][i].tree_cost < 0){
      operateFlatFunctionWithLearning(EPORCH, Tree[gen][i].postfix, &Tree[gen][i].postfix_size, LEAF_COST, &Tree[gen][i].Accuracy, Tree[gen][i].cost,\
                                      &Tree[gen][i].True_positive, &Tree[gen][i].False_positive, (int)th);
      cost = Tree[gen][i].cost[0];
      cost_index = 0;
      for(j = 1; j < Tree[gen][i].postfix_size; j++){
        if(Tree[gen][i].cost[j] < cost){
          cost = Tree[gen][i].cost[j];
          cost_index = j;
        }
      }
      Tree[gen][i].best_node = findIndexMin(Tree[gen][i].postfix_size-1, 0, Tree[gen][i].cost);
      Tree[gen][i].worst_node = findIndexMax(Tree[gen][i].postfix_size-1, 0, Tree[gen][i].cost);
      Tree[gen][i].best_cost = Tree[gen][i].cost[Tree[gen][i].best_node];
      Tree[gen][i].worst_cost = Tree[gen][i].cost[Tree[gen][i].worst_node];
      Tree[gen][i].tree_cost = Tree[gen][i].cost[Tree[gen][i].postfix_size-1];
      tree_cost[i] = Tree[gen][i].tree_cost;
    }else{
      tree_cost[i] = Tree[gen][i].tree_cost;
    }
  }
  return 0;
}

void operation(GPTree **Tree, int size){
  progress = 0;
  pthread_t threads[NUM_THREADS];
  int rc, i;
  for(i = 0; i < NUM_THREADS; i++){
    rc = pthread_create(&threads[i], NULL, t_operation, (void*)i);
    if(rc){
      i--;
      continue;
    }
  }
  for(i = 0; i < NUM_THREADS; i++){
    pthread_join(threads[i], NULL);
  }
}

#else

int main(){
  char *error_message = malloc(2000);
  error_message[0] = 0;
  sprintf(error_message, "%s", "Error: not completed setting in setting.c\nmissing:\n");
  #ifndef VARIABLE_COST_ESTIMATE_TIMES
    sprintf(error_message, "%s%s", error_message, "\tVARIABLE_COST_ESTIMATE_TIMES\n");
  #endif
  #ifndef VARIABLE_ALLOCATE_SIZE
    sprintf(error_message, "%s%s", error_message, "\tVARIABLE_ALLOCATE_SIZE\n");
  #endif
  #ifndef OBJECT_ALLOCATE_SIZE
    sprintf(error_message, "%s%s", error_message, "\tOBJECT_ALLOCATE_SIZE\n");
  #endif
  #ifndef INPUT_FILE
    sprintf(error_message, "%s%s", error_message, "\tINPUT_FILE\n");
  #endif
  #ifndef EPORCH
    sprintf(error_message, "%s%s", error_message, "\tEPORCH\n");
  #endif
  #ifndef LEAF_COST
    sprintf(error_message, "%s%s", error_message, "\tLEAF_COST\n");
  #endif
  #ifndef POPULATION_SIZE
    sprintf(error_message, "%s%s", error_message, "\tPOPULATION_SIZE\n");
  #endif
  #ifndef BLUK_OBJECT_ALLOCATE_SIZE
    sprintf(error_message, "%s%s", error_message, "\tBLUK_OBJECT_ALLOCATE_SIZE\n");
  #endif
  #ifndef C_MULT
    sprintf(error_message, "%s%s", error_message, "\tC_MULT\n");
  #endif
  #ifndef GENERATION
    sprintf(error_message, "%s%s", error_message, "\tGENERATION\n");
  #endif
  #ifndef PASS_PARENT_SIZE
    sprintf(error_message, "%s%s", error_message, "\tPASS_PARENT_SIZE\n");
  #endif
  #ifndef MAXIMUM_RANDOM_TREE_DEPTH
    sprintf(error_message, "%s%s", error_message, "\tMAXIMUM_RANDOM_TREE_DEPTH\n");
  #endif
  #ifndef COST_FUNCTION
    sprintf(error_message, "%s%s", error_message, "\tCOST_FUNCTION\n");
  #endif
  #ifndef NUM_THREADS
    sprintf(error_message, "%s%s", error_message, "\tNUM_THREADS\n");
  #endif
  sprintf(error_message, "%s%s", error_message, "Program is run not successfully.\n");
  printf(error_message);
}

#endif
