#ifndef __INIT__
#define __INIT__

  #define KNRM  "\x1B[0m"
  #define KRED  "\x1B[31m"
  #define KGRN  "\x1B[32m"
  #define KYEL  "\x1B[33m"
  #define KBLU  "\x1B[34m"
  #define KMAG  "\x1B[35m"
  #define KCYN  "\x1B[36m"
  #define KWHT  "\x1B[37m"

  typedef struct {
    char **tree;
    char **postfix;
    int postfix_size;
    int *tree_index;
    int tree_size;
    double Accuracy;
    int True_positive;
    int False_positive;
    double *cost;
    double best_cost; //cost of function node
    double worst_cost;
    int best_node; //index
    int worst_node;
    double tree_cost;
  } GPTree;

  double ***input, ***cur_theta, ***output, ***r_input, ***x_input;
  double ***t_theta, ***moutput, ***o0, ***o1, ***o2, **inverse_dummy;
  double ***m_one, ***dummy, ***t_input, ***Jp, ***Jpp, ***grad;
  char ***comb, ****dcomb, *****ccomb;

  int terminal_env_size = 0, function_env_size = 0, var_row = 0, var_col = 0, expect_env_size = 0;
  double *terminal_env, **expect_env, **variable_env, **add_variable_env, **dom_variable_env;
  int term_alloced = 0, var_alloced = 0, func_alloced = 0, expect_alloced = 0, func_is_set = 0;
  char **variable_name, **ff_format, **fv_format, **vv_format;
  double max_variable_cost, min_variable_cost, sum_variable_prop = 0;
  double max_add_variable_cost, min_add_variable_cost, sum_add_variable_prop = 0;
  double max_dom_variable_cost, min_dom_variable_cost, sum_dom_variable_prop = 0;
  double *variable_cost, *variable_prop, *add_variable_cost, *add_variable_prop, *dom_variable_cost, *dom_variable_prop;
  int ff = 0, fv = 0, vv = 0, matrix_dummy_var_size[NUM_THREADS];
  int cur_theta_var_size[NUM_THREADS], output_var_size[NUM_THREADS], r_input_var_size[NUM_THREADS], x_input_var_size[NUM_THREADS];
  int t_theta_var_size[NUM_THREADS], moutput_var_size[NUM_THREADS], o0_var_size[NUM_THREADS], o1_var_size[NUM_THREADS], o2_var_size[NUM_THREADS];
  int m_one_var_size[NUM_THREADS], dummy_var_size[NUM_THREADS], t_input_var_size[NUM_THREADS], Jp_var_size[NUM_THREADS], Jpp_var_size[NUM_THREADS];
  int grad_var_size[NUM_THREADS], dcomb_var_size[NUM_COMB_OBJECT], dcomb_var_size[NUM_COMB_OBJECT];

  double extract_inp[] = EXTRACT_VALUE_INPUT, extract_to[][2] = EXTRACT_VALUE_TO;
  double extract_inp_size = sizeof(extract_inp)/sizeof(extract_inp[0]);
  double extract_to_size = sizeof(extract_to)/sizeof(extract_to);
  double ****matrix_dummy;

  void init(){
    int i, j, k;
    variable_name = malloc(OBJECT_ALLOCATE_SIZE);
    variable_cost = malloc(OBJECT_ALLOCATE_SIZE);
    variable_prop = malloc(OBJECT_ALLOCATE_SIZE);
    add_variable_cost = malloc(OBJECT_ALLOCATE_SIZE);
    add_variable_prop = malloc(OBJECT_ALLOCATE_SIZE);
    dom_variable_cost = malloc(OBJECT_ALLOCATE_SIZE);
    dom_variable_prop = malloc(OBJECT_ALLOCATE_SIZE);
    variable_env = malloc(VARIABLE_ALLOCATE_SIZE*VARIABLE_ALLOCATE_SIZE);
    add_variable_env = malloc(VARIABLE_ALLOCATE_SIZE*VARIABLE_ALLOCATE_SIZE);
    dom_variable_env = malloc(VARIABLE_ALLOCATE_SIZE*VARIABLE_ALLOCATE_SIZE);
    expect_env = malloc(VARIABLE_ALLOCATE_SIZE);

    matrix_dummy = malloc(NUM_THREADS*BLUK_OBJECT_ALLOCATE_SIZE);

    input = malloc(NUM_THREADS*BLUK_OBJECT_ALLOCATE_SIZE);
    cur_theta = malloc(NUM_THREADS*OBJECT_ALLOCATE_SIZE);
    output = malloc(NUM_THREADS*OBJECT_ALLOCATE_SIZE);
    r_input = malloc(NUM_THREADS*BLUK_OBJECT_ALLOCATE_SIZE);
    x_input = malloc(NUM_THREADS*BLUK_OBJECT_ALLOCATE_SIZE);
    t_theta = malloc(NUM_THREADS*OBJECT_ALLOCATE_SIZE);
    moutput = malloc(NUM_THREADS*OBJECT_ALLOCATE_SIZE);
    o0 = malloc(NUM_THREADS*OBJECT_ALLOCATE_SIZE);
    o1 = malloc(NUM_THREADS*OBJECT_ALLOCATE_SIZE);
    o2 = malloc(NUM_THREADS*BLUK_OBJECT_ALLOCATE_SIZE);
    m_one = malloc(NUM_THREADS*OBJECT_ALLOCATE_SIZE);
    dummy = malloc(NUM_THREADS*OBJECT_ALLOCATE_SIZE);
    t_input = malloc(NUM_THREADS*BLUK_OBJECT_ALLOCATE_SIZE);
    Jp = malloc(NUM_THREADS*BLUK_OBJECT_ALLOCATE_SIZE);
    Jpp = malloc(NUM_THREADS*OBJECT_ALLOCATE_SIZE);
    grad = malloc(NUM_THREADS*OBJECT_ALLOCATE_SIZE);
    comb = malloc(NUM_THREADS*COMB_OBJECT_ALLOCATE_SIZE);
    dcomb = malloc(NUM_THREADS*NUM_COMB_OBJECT*COMB_OBJECT_ALLOCATE_SIZE);
    ccomb = malloc(2*NUM_THREADS*NUM_COMB_OBJECT*COMB_OBJECT_ALLOCATE_SIZE);
    inverse_dummy = malloc(NUM_THREADS*BLUK_OBJECT_ALLOCATE_SIZE);

    for(i = 0; i < NUM_THREADS; i++){
      cur_theta[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      cur_theta[i][0] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      input[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      output[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      r_input[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      x_input[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      t_theta[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      moutput[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      o0[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      o1[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      o2[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      m_one[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      t_input[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      Jp[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      Jpp[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      grad[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      dummy[i] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_ROW*NUM_MATRIX_ALLOCATE_COL);
      matrix_dummy[i] = malloc(2*sizeof(double)*NUM_MATRIX_ALLOCATE_DUMMY*NUM_MATRIX_ALLOCATE_DUMMY);
      comb[i] = malloc(COMB_OBJECT_ALLOCATE_SIZE);

      for(k = 0; k < NUM_MATRIX_ALLOCATE_ROW; k++){
        cur_theta[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        input[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        output[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        r_input[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        x_input[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        t_theta[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        moutput[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        o0[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        o1[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        o2[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        m_one[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        dummy[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_DUMMY);
        t_input[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        Jp[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        Jpp[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
        grad[i][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_COL);
      }
      matrix_dummy[i][0] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_DUMMY*NUM_MATRIX_ALLOCATE_DUMMY);
      matrix_dummy[i][1] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_DUMMY*NUM_MATRIX_ALLOCATE_DUMMY);
      for(k = 0; k < NUM_MATRIX_ALLOCATE_DUMMY; k++){
        matrix_dummy[i][0][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_DUMMY);
        matrix_dummy[i][1][k] = malloc(sizeof(double)*NUM_MATRIX_ALLOCATE_DUMMY);
      }

      for(k = 0; k < NUM_COMB_OBJECT_ALLOCATE; k++){
        comb[i][k] = malloc(COMB_ALLOCATE_SIZE);
      }

      dcomb[i] = malloc(NUM_COMB_OBJECT*COMB_OBJECT_ALLOCATE_SIZE);
      ccomb[i] = malloc(NUM_COMB_OBJECT*COMB_OBJECT_ALLOCATE_SIZE);

      for(j = 0; j < NUM_COMB_OBJECT; j++){
        dcomb[i][j] = malloc(COMB_OBJECT_ALLOCATE_SIZE);
        ccomb[i][j] = malloc(COMB_OBJECT_ALLOCATE_SIZE);
        ccomb[i][j][0] = malloc(COMB_OBJECT_ALLOCATE_SIZE);
        ccomb[i][j][1] = malloc(COMB_OBJECT_ALLOCATE_SIZE);
        for(k = 0; k < NUM_COMB_OBJECT_ALLOCATE; k++){
          dcomb[i][j][k] = malloc(COMB_ALLOCATE_SIZE);
          ccomb[i][j][0][k] = malloc(COMB_ALLOCATE_SIZE);
          ccomb[i][j][1][k] = malloc(COMB_ALLOCATE_SIZE);
        }
      }

      cur_theta_var_size[i] = 1;
      output_var_size[i] = 0;
      r_input_var_size[i] = 0;
      x_input_var_size[i] = 0;
      t_theta_var_size[i] = 0;
      moutput_var_size[i] = 0;
      o0_var_size[i] = 0;
      o1_var_size[i] = 0;
      o2_var_size[i] = 0;
      m_one_var_size[i] = 0;
      dummy_var_size[i] = 0;
      t_input_var_size[i] = 0;
      Jp_var_size[i] = 0;
      Jpp_var_size[i] = 0;
      grad_var_size[i] = 0;
    }

    terminal_env_size = 0;
    function_env_size = 0;
    var_row = 0;
    var_col = 0;
    expect_env_size = 0;
  }

#endif
